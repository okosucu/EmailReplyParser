package octopus.chat;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import octopus.chat.model.EmailItemRequest;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static io.restassured.RestAssured.given;
import static octopus.chat.model.EmailItem.Types.PLAIN;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MessagePostIT {


    @Test
    @Order(1)
    public void testEmailItemCreate() {
        EmailItemRequest emailItemRequest = new EmailItemRequest("email_1.txt","okosucu@gmail.com","okosucu@gmail.com",PLAIN);
        given().basePath("/api/email")
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .when()
                .body(emailItemRequest)
                .post("/emailItem.create")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .prettyPeek();
    }

    @Test
    @Order(2)
    public void testEmailItemCreateEmail2() {
        EmailItemRequest emailItemRequest = new EmailItemRequest("email_2.txt","okosucu@gmail.com","okosucu@gmail.com",PLAIN);
        given().basePath("/api/email")
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .when()
                .body(emailItemRequest)
                .post("/emailItem.create")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .prettyPeek();
    }


    @Test
    @Order(3)
    public void testGetEmailItems() {
        given().basePath("/api/email")
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .when().get("/emailItem.list")
                .then()
                .statusCode(200)
                .extract()
                .response()
                .prettyPeek();
    }

}