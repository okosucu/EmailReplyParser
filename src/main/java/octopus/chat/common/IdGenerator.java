package octopus.chat.common;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

import java.io.Serializable;
import java.security.SecureRandom;
import java.util.UUID;

public final class IdGenerator implements Serializable {

    private static final char[] ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private static final SecureRandom random = new SecureRandom();
    private static final Integer DEFAULT_SIZE = 10;

    public static synchronized String generateUid() {
        return UUID.randomUUID().toString();
    }

    public static synchronized String generate() {
        return generate(DEFAULT_SIZE);
    }

    public static synchronized String generate(String prefix) {
        return generate(prefix, DEFAULT_SIZE);
    }

    public static synchronized String generate(int length) {
        return NanoIdUtils.randomNanoId(random, ALPHABET, length);
    }

    public static synchronized String generate(String prefix, int length) {
        return prefix + "_" + NanoIdUtils.randomNanoId(random, ALPHABET, length);
    }

}
