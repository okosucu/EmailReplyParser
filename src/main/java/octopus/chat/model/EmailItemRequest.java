package octopus.chat.model;

import javax.validation.constraints.NotEmpty;

import static octopus.chat.model.EmailItem.Types.PLAIN;


public class EmailItemRequest {


    @NotEmpty(message = "{EmailItem.FileName.notEmpty}")
    public String fileName;

    @NotEmpty(message = "{EmailItem.SenderAddress.notEmpty}")
    public String senderAddress;

    @NotEmpty(message = "{EmailItem.RecipientAddress.notEmpty}")
    public String recipientAddress;


    @NotEmpty(message = "{EmailItem.EmailType.notEmpty}")
    public String emailType = PLAIN;

    public EmailItemRequest() {
    }

    public EmailItemRequest( String fileName, String senderAddress,String recipientAddress, String emailType) {
        this.fileName = fileName;
        this.senderAddress = senderAddress;
        this.emailType = emailType;
        this.recipientAddress=recipientAddress;
    }
}
