package octopus.chat.model;

public class EmailReplyParser {

	public static EmailItem read(String emailText) {
		if (emailText == null)
			emailText = "";

		EmailParser parser = new EmailParser();
		return parser.parse(emailText);
	}
	
	public static String parseReply(String emailText) {
		return read(emailText).getVisibleText();
	}

}
