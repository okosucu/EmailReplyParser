package octopus.chat.model;

import java.util.List;


public class FragmentDTO {
	public List<String> lines;
	public boolean isHidden = false;
	public boolean isSignature = false;
	public boolean isQuoted = false;
}
