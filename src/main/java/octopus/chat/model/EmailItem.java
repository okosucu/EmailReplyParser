package octopus.chat.model;

import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class EmailItem {

    public String id;
    public String senderAddress;
    public String recipientAddress;
    public String type;
    public Instant time;

    public List<Fragment> fragments = new ArrayList<Fragment>();

    public EmailItem(List<Fragment> fragments) {
        this.fragments = fragments;
    }

    public EmailItem() {
    }

    public String getId() {
        return id;
    }

    public EmailItem setId(String id) {
        this.id = id;
        return this;
    }

    public List<Fragment> getFragments() {
        return fragments;
    }

    public EmailItem setFragments(List<Fragment> fragments) {
        this.fragments = fragments;
        return this;
    }

    public String getVisibleText() {
        List<String> visibleFragments = new ArrayList<String>();
        for (Fragment fragment : fragments) {
            if (!fragment.isHidden())
                visibleFragments.add(fragment.getContent());
        }
        return StringUtils.stripEnd(StringUtils.join(visibleFragments, "\n"), null);
    }

    public String getHiddenText() {
        List<String> hiddenFragments = new ArrayList<String>();
        for (Fragment fragment : fragments) {
            if (fragment.isHidden())
                hiddenFragments.add(fragment.getContent());
        }
        return StringUtils.stripEnd(StringUtils.join(hiddenFragments, "\n"), null);
    }

    public boolean includeParentEmailItem(EmailItem parent) {
        return time.isAfter(parent.time);
    }

    public EmailItem setSenderAddress(String senderAddress) {
        this.senderAddress = senderAddress;
        return this;
    }

    public EmailItem setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
        return this;
    }

    public EmailItem setType(String type) {
        this.type = type;
        return this;
    }

    public EmailItem setTime(Instant time) {
        this.time = time;
        return this;
    }

    public interface Types {
        String PLAIN = "email.plainText";
        String RICH = "email.richText";
    }
}
