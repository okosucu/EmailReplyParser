package octopus.chat.model;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;


@Path("/email")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public interface EmailItemApi {

    @GET
    @Path("/emailItem.get")
    EmailItem get(@NotEmpty String emailId);

    @GET
    @Path("/emailItem.list")
    List<EmailItem> list();

    @POST
    @Path("/emailItem.create")
    Response create(@NotNull @Valid EmailItemRequest emailItemRequest);

}
