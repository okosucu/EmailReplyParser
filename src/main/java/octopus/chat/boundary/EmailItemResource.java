package octopus.chat.boundary;

import octopus.chat.model.EmailItem;
import octopus.chat.model.EmailItemApi;
import octopus.chat.model.EmailItemRequest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;


@RequestScoped
@SuppressWarnings("unused")
public class EmailItemResource implements EmailItemApi {

    @Inject
    EmailItemService service;

    @Override
    public EmailItem get(String emailId)
    {
        return service.getEmailItemById(emailId);
    }


    @Override
    public List<EmailItem> list()
    {
        return service.getEmailItemList();
    }


    @Override
    public Response create(EmailItemRequest emailItemRequest)
    {
        return Response.ok(service.createEmailItem(emailItemRequest.senderAddress,emailItemRequest.recipientAddress,emailItemRequest.fileName,emailItemRequest.emailType)).build();
    }
}
