package octopus.chat.boundary;


import octopus.chat.common.FixtureGetter;
import octopus.chat.common.IdGenerator;
import octopus.chat.entity.EmailItemEntity;
import octopus.chat.entity.EmailItemJsonEntity;
import octopus.chat.model.*;

import javax.enterprise.context.Dependent;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;

@Dependent
public class EmailItemService {

    public EmailItem createEmailItem(String senderAddress, String recipientAddress,String fileName, String type) {
        List<Fragment> fragmentList = new EmailParser().parse(FixtureGetter.getFixture(fileName)).getFragments();
        EmailItem emailItem = new EmailItem().setSenderAddress(senderAddress)
                .setRecipientAddress(recipientAddress)
                .setFragments(fragmentList)
                .setType(type);
        dbPersist(emailItem);
        return emailItem;
    }

    @Transactional
    void dbPersist(EmailItem emailItem) {
        if (emailItem.getId() == null) {
            emailItem.setId(IdGenerator.generate(16));
        }
        if (emailItem.time == null) {
            emailItem.time = Instant.now();
        }

        new EmailItemEntity(emailItem.id, emailItem).setParentId(getParentEmailItemId(emailItem)).persist();
        new EmailItemJsonEntity(emailItem.id, emailItem.getVisibleText()).persist();
    }


    private String getParentEmailItemId(EmailItem emailItem)
    {
        for (EmailItem parentEmailItem : getEmailItemList())
            if (emailItem.includeParentEmailItem(parentEmailItem))
                return parentEmailItem.id;
        return null;
    }
    public List<EmailItem> getEmailItemList() {
        return EmailItemEntity.getEmailItemList();
    }


    public EmailItem getEmailItemById(String emailId) {
        return EmailItemEntity.getEmailItemById(emailId);
    }
}
