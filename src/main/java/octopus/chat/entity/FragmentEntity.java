package octopus.chat.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import octopus.chat.model.EmailItem;
import octopus.chat.model.Fragment;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity(name = "Fragments")
@Cacheable
@Table(name = "FRAGMENTS")
public class FragmentEntity extends PanacheEntityBase {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    public String id;

    @NotEmpty(message = "Fragment.content.notEmpty")
    @Column(name = "content", length = 1024, nullable = false, updatable = false)
    public String content;

    @Column(name = "isHidden", nullable = false, updatable = false)
    public boolean isHidden;

    @Column(name = "isSignature", nullable = false, updatable = false)
    public boolean isSignature;

    @Column(name = "isQuoted", nullable = false, updatable = false)
    public boolean isQuoted;

    @ManyToOne(fetch = FetchType.LAZY)
    private EmailItemEntity emailItem;


    public FragmentEntity() {
    }

    public FragmentEntity(String id, String content, boolean isHidden, boolean isSignature, boolean isQuoted, EmailItemEntity emailItemEntity) {
        this.id = id;
        this.content = content;
        this.isHidden = isHidden;
        this.isSignature = isSignature;
        this.isQuoted = isQuoted;
        this.emailItem = emailItemEntity;
    }

    public Fragment toFragment() { return new Fragment(content,isHidden,isSignature,isQuoted);}

    public EmailItemEntity getEmailItem() {
        return emailItem;
    }

    public void setEmailItem(EmailItemEntity emailItem) {
        this.emailItem = emailItem;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }

    public void setSignature(boolean signature) {
        isSignature = signature;
    }

    public void setQuoted(boolean quoted) {
        isQuoted = quoted;
    }
}
