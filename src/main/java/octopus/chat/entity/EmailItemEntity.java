package octopus.chat.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.runtime.JpaOperations;
import io.quarkus.panache.common.Parameters;
import octopus.chat.model.EmailItem;
import octopus.chat.common.IdGenerator;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity(name = "Email")
@Cacheable
@Table(name = "EMAILS")
@NamedNativeQuery(
        name = "getEmails",
        query = "with recursive Email_Tree(id,parent_id,level,sender) as ("
                + " select email_id, parent_id,1,sender"
                + " from EMAILS"
                + " where parent_id is null"
                + " union all"
                + " select e.email_id, e.parent_id,t.level+1,e.sender"
                + " from EMAILS e"
                + " join Email_Tree t on t.id=e.parent_id)"
                + " select e.* "
                + " from EMAILS e"
                + " join Email_Tree t on e.email_id = t.id"
                + " join FRAGMENTS f on t.id = f.EMAILITEM_EMAIL_ID"
                + " and f.ISHIDDEN =false"
                + " and f.ISSIGNATURE=false",
        resultClass = EmailItemEntity.class)


public class EmailItemEntity extends PanacheEntityBase {

    @Id
    @Column(name = "email_id", nullable = false, updatable = false)
    public String id;

    @Email
    @NotEmpty(message = "FileName.senderAddress.notEmpty")
    @Column(name = "sender", nullable = false, updatable = false)
    public String senderAddress;

    @Email
    @NotEmpty(message = "FileName.recipientAddress.notEmpty")
    @Column(name = "recipient", nullable = false, updatable = false)
    public String recipientAddress;

    @NotEmpty(message = "FileName.type.notEmpty")
    @Column(name = "type", nullable = false, updatable = false)
    public String type;

    @Column(name = "time", nullable = false, updatable = false)
    public Instant time;

    @Column(name = "parent_id", updatable = false)
    public String parentId;

    @OneToMany(mappedBy = "emailItem", cascade = CascadeType.ALL)
    private List<FragmentEntity> fragments = new ArrayList<>();


    public EmailItemEntity() {
    }

    public EmailItemEntity(String id, EmailItem email) {
        this.id = id;
        this.senderAddress = email.senderAddress;
        this.recipientAddress = email.recipientAddress;
        this.type = email.type;
        this.time = email.time;
        addFragments(email);
    }

    public static EmailItem getEmailItemById(String emailId) {

        String querySql = "with recursive Email_Tree(id,sender,parent_id) as "
                + " (select id, sender,parent_id "
                + " from Email"
                + " where parent_id = :emailId"
                + " union all"
                + " select e2.id, e2.sender, e2.parent_id"
                + " from Email e2"
                + " inner join Email_Tree t on Email_Tree.id=e2.parent_id"
                + " )"
                + " select j "
                + " from EmailJSon j"
                + " join Email_Tree t on j.id = t.id"
                + " join Fragment f on t.id = f.emailItem"
                + " order by sender";


        return EmailItemJsonEntity.find(querySql, Parameters.with("emailId", emailId))
                .page(0, 50)
                .<EmailItemJsonEntity>list()
                .stream()
                .map(EmailItemJsonEntity::toEmailItem)
                .findFirst()
                .orElse(null);
    }

    public static List<EmailItem> getEmailItemList() {

        return JpaOperations.getEntityManager()
                .createNamedQuery("getEmails", EmailItemEntity.class)
                .getResultList()
                .stream()
                .map(EmailItemEntity::toEmailItem)
                .collect(Collectors.toList());
    }

    private void addFragments(EmailItem email) {
        email.fragments.stream().forEach(fragment -> this.fragments.add(new FragmentEntity(IdGenerator.generate(16), fragment.getContent(), fragment.isHidden(), fragment.isSignature(), fragment.isQuoted(), this)));
    }

    public EmailItem toEmailItem() {
        return new EmailItem().setId(id).setTime(time).setRecipientAddress(recipientAddress).setSenderAddress(senderAddress).setType(type).setFragments(fragments.stream().map(FragmentEntity::toFragment).collect(Collectors.toList()));
    }

    public EmailItemEntity setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    @PrePersist
    void prePersist() {
        if (time == null) {
            time = Instant.now();
        }
    }

}