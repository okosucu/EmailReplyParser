package octopus.chat.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.vertx.core.json.Json;
import octopus.chat.model.EmailItem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "EmailJson")
@Table(name = "EMAIL_JSON")
public class EmailItemJsonEntity extends PanacheEntityBase {
    @Id
    @Column(name = "id", nullable = false, updatable = false)
    private String id;


    @Column(name = "email", length = 1024, columnDefinition = "text")
    private String json;

    public EmailItemJsonEntity() {
    }

    public EmailItemJsonEntity(String id, String email) {
        this.id = id;
        this.json = Json.encode(email);
    }

    public EmailItem toEmailItem() {
        return Json.decodeValue(json, EmailItem.class);
    }

    public String getJson() {
        return json;
    }

    public EmailItemJsonEntity setJson(String json) {
        this.json = json;
        return this;
    }
}
